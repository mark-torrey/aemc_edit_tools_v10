﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using ESRI.ArcGIS.Editor;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Framework;

namespace AEMC.EditWorkflow_10
{
    class fieldHelper
    {
        #region FIELDS

        string fieldName = null;
        int fieldIndex = -1;

        #endregion

        #region PROPERTIES

        public string Name
        {
            get
            {
                return fieldName;
            }
        }

        public int FieldIndex
        {
            get
            {
                return fieldIndex;
            }
        }

        public bool Exists
        {
            get
            {
                return fieldIndex > -1;
            }
        }

        #endregion

        #region METHODS

        /*
             * costructor
             */
        public fieldHelper(string name, int ndx)
        {
            if (name == null || name.Length == 0)
                name = string.Empty;

            fieldName = name;
            fieldIndex = ndx;
        }

        #endregion
    }
    
    /// <summary>
    /// EditorExtension class implementing custom ESRI Editor Extension functionalities.
    /// </summary>
    public class EditController : ESRI.ArcGIS.Desktop.AddIns.Extension
    {
        #region FIELDS

        private EditWorkflowController _ext = null;
        private bool _writelog = false;
        private ITable _logTable = null;
        
        #endregion

        #region PROPERTIES

        /*
         * 
         */
        private bool writelog
        {
            get
            {
                return _writelog;
            }
            set
            {
                _writelog = value;
            }
        }

        /*
         * 
         */
        private ITable logtable
        {
            get
            {
                return _logTable;
            }
            set
            {
                _logTable = value;
            }
        }

        /*
         * 
         */
        private IEditEvents_Event Events
        {
            get { return ArcMap.Editor as IEditEvents_Event; }
        }

        /*
         * 
         */
        private IEditEvents2_Event Events2
        {
            get { return ArcMap.Editor as IEditEvents2_Event; }
        }

        /*
         * 
         */
        private IEditEvents3_Event Events3
        {
            get { return ArcMap.Editor as IEditEvents3_Event; }
        }

        /*
         * 
         */
        private IEditEvents4_Event Events4
        {
            get { return ArcMap.Editor as IEditEvents4_Event; }
        }

        #endregion

        #region METHODS

        /*
         * constructor
         */
        public EditController()
        {
        }

        /*
         * Editor Extension startup event handler
         */
        protected override void OnStartup()
        {
            IEditor theEditor = ArcMap.Editor;
            WireEditorEvents();

            _ext = EditWorkflowController.GetExtension();

        }

        /*
         * Editor Extension shutdown event handler
         */
        protected override void OnShutdown()
        {
        }

        /*
         * 
         */
        private void WireEditorEvents()
        {
            Events.OnChangeFeature += EditFeatureDidChange;
            Events.OnCreateFeature += EditFeatureWasCreated;
            Events.OnDeleteFeature += EditFeatureWasDeleted;
            Events.OnStartEditing += EditSessionDidStart;
            Events.OnStopEditing += EditSessionDidStop;
        }

        /*
         * Check if this is a layer we want to manage
         */
        private bool isExtLayer(IObject obj)
        {
            string className = ((IDataset)obj.Class).Name.ToUpper();
            return _ext.AppConfig.getLayerNames().Contains(className);
        }

        /*
         * Check to see if this is field we want to populate
         */
        private fieldHelper hasField(IObject obj, string fieldname)
        {
            int fldNdx = obj.Fields.FindField(fieldname);
            string fldName = string.Empty;
            if (fldNdx > -1)
            {
                fldName = obj.Fields.get_Field(fldNdx).Name;
            }
            return new fieldHelper(fldName, fldNdx);
            
        }

        /*
         * 
         */
        private void updateFeature(IObject obj, string edittype)
        {
            if (isExtLayer(obj) && edittype != "Delete")
            {
                fieldHelper designField = hasField(obj, _ext.AppConfig.WorkOrderIdField);
                if (designField.Exists)
                {
                    obj.set_Value(designField.FieldIndex, _ext.CurrentEditVersionName);
                }

                fieldHelper workidField = hasField(obj, _ext.AppConfig.ServiceOrderFieldName);
                if (workidField.Exists)
                {
                    obj.set_Value(workidField.FieldIndex, _ext.CurrentServiceOrderValue);
                }
            }

            if (writelog && logtable != null)
            {
                logedit(obj, edittype);
            }
        }

        /*
         * 
         */
        private void logedit(IObject obj, string edittype)
        {
            IRow newRow = logtable.CreateRow();
            newRow.set_Value(newRow.Fields.FindField("edittype"), edittype);
            newRow.set_Value(newRow.Fields.FindField("editorname"), Environment.UserName);
            newRow.set_Value(newRow.Fields.FindField("editordate"), DateTime.Now);
            newRow.set_Value(newRow.Fields.FindField("workorder"), _ext.CurrentEditVersionName);
            newRow.set_Value(newRow.Fields.FindField("serviceorder"), _ext.CurrentServiceOrderValue);
            newRow.set_Value(newRow.Fields.FindField("layername"), ((IDataset)obj.Class).Name);
            newRow.set_Value(newRow.Fields.FindField("machinename"), Environment.MachineName);
            if (obj is IFeature)
            {
                newRow.set_Value(newRow.Fields.FindField("centroid_x"), ((IGeometry5)((IFeature)obj).Shape).CentroidEx.X);
                newRow.set_Value(newRow.Fields.FindField("centroid_y"), ((IGeometry5)((IFeature)obj).Shape).CentroidEx.Y);
            }

            List<esriFieldType> fTypes = new List<esriFieldType>(){
                        esriFieldType.esriFieldTypeGeometry,
                        esriFieldType.esriFieldTypeGlobalID,
                        esriFieldType.esriFieldTypeGUID,
                        esriFieldType.esriFieldTypeOID,
                        esriFieldType.esriFieldTypeRaster,
                    };
            ArrayList ov = new ArrayList();
            ArrayList nv = new ArrayList();
            for (int f = 0; f < obj.Fields.FieldCount; f++)
            {
                if (((IRowChanges)obj).get_ValueChanged(f))
                {
                    if (!fTypes.Contains(obj.Fields.get_Field(f).Type))
                    {
                        ov.Add(string.Format("{0}:{1}", obj.Fields.get_Field(f).Name, ((IRowChanges)obj).get_OriginalValue(f).ToString()));
                        nv.Add(string.Format("{0}:{1}", obj.Fields.get_Field(f).Name, obj.get_Value(f)));
                    }
                }
            }

            if (ov.Count > 0)
            {
                newRow.set_Value(newRow.Fields.FindField("preeditvalues"), string.Join(",", (string[])ov.ToArray(typeof(string))));
            }

            if (nv.Count > 0)
            {
                newRow.set_Value(newRow.Fields.FindField("posteditvalues"), string.Join(",", (string[])nv.ToArray(typeof(string))));
            }

            newRow.Store();
        }

        /*
         * 
         */
        private void EditFeatureWasCreated(IObject obj)
        {
            updateFeature(obj, "Create");   
        }

        /*
         * 
         */
        private void EditFeatureDidChange(IObject obj)
        {
            updateFeature(obj, "Update");
        }

        /*
         * 
         */
        private void EditFeatureWasDeleted(IObject obj)
        {
            updateFeature(obj, "Delete");
        }

        /*
         * Edit Session started
         */
        private void EditSessionDidStart()
        {
            if (_ext.AppConfig.LogTableName == null || _ext.AppConfig.LogTableName.Length <= 0)
            {
                writelog = false;
                return;
            }

            IEnumDatasetName dsNames = ArcMap.Editor.EditWorkspace.get_DatasetNames(esriDatasetType.esriDTTable);
            IDatasetName dsName = dsNames.Next();
            while (dsName != null)
            {
                if (dsName.Name.ToUpper() == _ext.AppConfig.LogTableName.ToUpper())
                {
                    writelog = true;
                    logtable = (ITable)((IName)dsName).Open();
                    break;
                }
                dsName = dsNames.Next();
            }

            UID createFeaturesUID = new UIDClass();
            createFeaturesUID.Value = "{B18ED544-C8B1-49F3-9F7A-F83CBE793CDB}";

            ICommandItem createFeaturesCommand = ArcMap.Application.Document.CommandBars.Find(createFeaturesUID,false,false);
            createFeaturesCommand.Execute();

            UID editAttributeUID = new UIDClass();
            editAttributeUID.Value = "{44276914-98C1-11D1-8464-0000F875B9C6}";
            
            ICommandItem editAttributesCommand = ArcMap.Application.Document.CommandBars.Find(editAttributeUID, false, false);
            editAttributesCommand.Execute();

            UID editToolUID = new UIDClass();
            editToolUID.Value = "{9EBF3A1E-D0C0-11D0-802D-0000F8037368}";
            ICommandItem editTool = ArcMap.Application.Document.CommandBars.Find(editToolUID, false, false);
            ArcMap.Application.CurrentTool = editTool;

            
        }

        /*
         * Edit Session Stopped
         */
        private void EditSessionDidStop(bool save)
        {
            
        }
        
        #endregion
    }
}
