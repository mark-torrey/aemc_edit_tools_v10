﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace AEMC.EditWorkflow_10
{
    /**
     * 
     **/
    class SettingsController
    {
        #region FIELDS


        #endregion

        #region PROPERTIES

        public Settings AppSettings { get; set; }

        #endregion

        #region METHODS

        /*
         * Constructor
         */
        public SettingsController()
        {

            string _configFileFullPath = getSettingsPath();
            if (System.IO.File.Exists(_configFileFullPath))
            {
                string j = File.ReadAllText(_configFileFullPath);
                string wj = System.Text.RegularExpressions.Regex.Replace(j, @"\s+", "");
                AppSettings = JsonConvert.DeserializeObject<Settings>(wj);
            }
            else
            {
                AppSettings = null;
                System.Console.WriteLine("File Not Found");
            }
        }

        /**
         * 
         **/
        private static string getSettingsPath()
        {
            string configFilePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string configFileFullPath = System.IO.Path.Combine(configFilePath, "config.json");
            return configFileFullPath;
        }

        /**
         * 
         **/
        internal static void WriteSettingsToFile(Settings s)
        {
            string json = JsonConvert.SerializeObject(s, Formatting.Indented);
            File.WriteAllText(getSettingsPath(), json);
        }

        #endregion
    }

    /**
     * Class represtenting the structure of our configuration file
     **/
    class Settings
    {
        public List<string> Layers { get; set; }
        public string WorkOrderIdField { get; set; }
        public string ServiceOrderFieldName { get; set; }
        public string LogTableName { get; set; }
        public bool LocalDatabase { get; set; }
        public string LocalWorkingDir { get; set; }

        public List<string> getLayerNames() {
            List<string> localLayerNames = new List<string>();
            if (LocalDatabase)
            {
                foreach (string lyrName in Layers)
                {
                   string lname = lyrName.Split(new string[]{"."}, StringSplitOptions.None)[2];
                   localLayerNames.Add(lname);
                }
                return localLayerNames;
            }
            else
            {
                return Layers;
            }

        }

    }
}
