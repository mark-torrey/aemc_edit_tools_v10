﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.esriSystem;

namespace AEMC.EditWorkflow_10
{
    public class StopEditingButton : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        EditWorkflowController _ext = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public StopEditingButton()
        {
            System.Diagnostics.Debug.WriteLine("AEMC StopEditing Constructor");
            _ext = EditWorkflowController.GetExtension();
            this.Enabled = false;
        }
        
        /// <summary>
        /// Button click event handler
        /// </summary>
        protected override void OnClick()
        {
            if (_ext.AppConfig.LocalDatabase)
            {
                _ext.stopLocalGDBEditSession();
            }
            else
            {
                _ext.ReconcilePostVersion();
            }
            
        }
        
        /// <summary>
        /// Command enable event handler
        /// </summary>
        protected override void OnUpdate()
        {
            Enabled = _ext.isEditing;
        }
    }

}
