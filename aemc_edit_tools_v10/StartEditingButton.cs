﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AEMC.EditWorkflow_10
{
    public class StartEditingButton : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        EditWorkflowController _ext = null;

        /**
         * Constructor
         **/
        public StartEditingButton()
        {
            System.Diagnostics.Debug.WriteLine("Start Editing Button Constructor");
            _ext = EditWorkflowController.GetExtension();
        }

        /**
         * button click event handler
         **/
        protected override void OnClick()
        {
            if (_ext.AppConfig.LocalDatabase)
            {
                _ext.CreateNewReplica();
            }
            else
            {
                _ext.CreateNewVersion();
            }
            
        }

        /**
         * button update event handler
         **/
        protected override void OnUpdate()
        {
            Enabled = _ext.isEnabled && !_ext.isEditing;
        }
    }
}
