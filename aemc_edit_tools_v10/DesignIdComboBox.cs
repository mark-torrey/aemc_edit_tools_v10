﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace AEMC.EditWorkflow_10
{
    public class DesignIdComboBox : ESRI.ArcGIS.Desktop.AddIns.ComboBox
    {
        #region FIELDS
        private EditWorkflowController _ext = null;
        #endregion

        #region PROPERTIES
        #endregion

        #region METHODS
       
        /*
         * constructor
         */
        public DesignIdComboBox()
        {
            _ext = EditWorkflowController.GetExtension();
        }

        /*
         * remove items
         */
        internal void clear()
        {
            this.Clear();
        }

        /*
         * add value to control
         */
        internal void AddItem(string value)
        {
            int i = this.Add(value);
            this.Select(i);
        }
        
        /*
         * should control be enabled or disabled 
         */
        protected override void OnUpdate()
        {
            this.Enabled = _ext.isEnabled && !_ext.isEditing;
        }

        /*
         * return value currently displayed in control
         */
        internal string GetText()
        {
            return this.Value;
        }
        
        #endregion
    }
}
