﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace AEMC.EditWorkflow_10
{
    public class CreateLocalDatabaseButton : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        #region FIELDS
        private EditWorkflowController _ext = null;
        #endregion

        #region PROPERTIES
        #endregion

        #region METHODS

        /**
         * constructor
         **/
        public CreateLocalDatabaseButton()
        {
            _ext = EditWorkflowController.GetExtension();
        }

        /**
         * Button click event handler
         **/
        protected override void OnClick()
        {
            _ext.CreateNewReplica();
        }

        /**
         * Button update event handler
         **/
        protected override void OnUpdate()
        {
        }

        #endregion
       
    }
}
